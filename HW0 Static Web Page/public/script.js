var slideIndex = 0;

function LClick() {
  let i;
  var slides = document
    .getElementsByClassName("slideshow")[0]
    .getElementsByClassName("slidepage");

  // ซ่อนรูปทั้งหมด
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  // เปลี่ยนแสดงรูปตามลำดับ
  slideIndex--;
  if (slideIndex < 1) {
    slideIndex = slides.length;
  }

  slides[slideIndex - 1].style.display = "block";

  // แสดงรูปถัดไปทุก 3 วินาที
}
function RClick() {
  let i;
  var slides = document
    .getElementsByClassName("slideshow")[0]
    .getElementsByClassName("slidepage");

  // ซ่อนรูปทั้งหมด
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  // เปลี่ยนแสดงรูปตามลำดับ
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }

  slides[slideIndex - 1].style.display = "block";

  // แสดงรูปถัดไปทุก 3 วินาที
}
function showSlides() {
  let i;
  var slides = document
    .getElementsByClassName("slideshow")[0]
    .getElementsByClassName("slidepage");

  // ซ่อนรูปทั้งหมด
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  // เปลี่ยนแสดงรูปตามลำดับ
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }

  slides[slideIndex - 1].style.display = "block";

  // แสดงรูปถัดไปทุก 3 วินาที
  setTimeout(showSlides, 3000);
}
showSlides();
