<?php include 'connect.php'; 
    session_start();
    $sql = "SELECT * FROM orders";
    if(!$_SESSION['IsAdmin']){
        $sql .= " WHERE username = '".$_SESSION["username"]."'";
    }
    if(isset($_POST['username'])){
        $sql .= " WHERE username = '".$_POST["username"]."'";
    }
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table,td,th{
            border: 1px solid;
        }
        td,th{
            padding:0.5rem 1rem;
        }
        input[type="submit"]{
            border: none;
            background:none;
            color:blue;
            cursor: pointer;
        }
        input[type="submit"]:hover{
            color:gray;
        }
    </style>
</head>
<body>
    <h3><?= "Username : ".$_SESSION['username'];?></h3>
<table>
        <thead>
            <tr>
                <th>ORDER ID</th>
                <th>USERNAME</th>
                <th>DATE</th>
                <th>STATUS</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if ($stmt){
            while ($row = $stmt->fetch()) {
                ?>
                <tr>
                    <td><?= $row['ord_id']; ?></td>
                    <?php if($_SESSION['IsAdmin']) { ?>
                        <form action="#" method="POST">
                            <input type="hidden" value=<?= $row['username'] ?> name='username'/>
                            <td><input type="submit" value=<?= $row['username'] ?>></td>
                        </form>
                    <?php } else {?>
                        <td><?= $row['username'] ?></td>
                    <?php }?>
                    <td><?= $row['username']; ?></td>
                    <td><?= $row['ord_date']; ?></td>
                    <td><?= $row['status']; ?></td>
                </tr>
                <?php
            }
        } else {
            echo 'Failed to query';
        }
        ?>
        </tbody>
    </table>
</body>
</html>