<?php include 'connect.php'; 

    $stmt = $pdo->prepare("SELECT * FROM member WHERE username = ? AND password = ?");
    $stmt->bindParam(1, $_POST["username"]);
    $stmt->bindParam(2, $_POST["password"]);
    $stmt->execute();
    $row = $stmt->fetch();

    if (empty($row)){
        header( "/login.php" );
        exit(0);
    } 
 
    $sql = "SELECT * FROM orders";
    if(!$row['IsAdmin']){
        $sql .= " WHERE username = '".$_POST["username"]."'";
    }
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    </style>
</head>
<body>
<table>
        <thead>
            <tr>
                <th>ORDER ID</th>
                <th>USERNAME</th>
                <th>DATE</th>
                <th>STATUS</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if ($stmt){
            while ($row = $stmt->fetch()) {
                ?>
                <tr>
                    <td><?php echo $row['ord_id']; ?></td>
                    <td><?php echo $row['username']; ?></td>
                    <td><?php echo $row['ord_date']; ?></td>
                    <td><?php echo $row['status']; ?></td>
                </tr>
                <?php
            }
        } else {
            echo 'Failed to query';
        }
        ?>
        </tbody>
    </table>
</body>
</html>