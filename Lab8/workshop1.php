<?php include 'connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table , td ,th{
            border:1px solid;
            text-align:center;
        }
        td ,th{
            padding:5px;;
        }
    </style>
</head>
<body>
    <?php
    $stmt = $pdo->prepare('SELECT * FROM product');
    $stmt->execute();
    ?>
    <table>
        <thead>
            <tr>
                <th>รหัสสินค้า</th>
                <th>ชื่อสินค้า</th>
                <th>รายละเอียด</th>
                <th>ราคา</th>
            </tr>
        </thead>
        <tbody>
        <?php while ($row = $stmt->fetch()) { ?>
            <tr>
                <td><?php echo $row["pid"] ?></td>
                <td><?php echo $row["pname"] ?></td>
                <td><?php echo $row["pdetail"] ?></td>
                <td><?php echo $row["price"] ?></td>
            </tr>
            
        <?php } ?>
        </tbody>
    </table>
    
</body>
</html>