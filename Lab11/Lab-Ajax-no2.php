<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        async function searchUsername() {
            const keyword = document.getElementById("username").value;
            const url = "query.php?username=" + keyword;
            
            const request = new XMLHttpRequest();
            request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status == 200) {
                    document.getElementById("result").innerHTML = request.responseText;
                }
            };
            
            request.open("GET", url, true);
            request.send();
        }
    </script>
</head>
<body>
    Search : <input type="text" id="username" name="username">
    <button onclick="searchUsername()">submit</button>   
    <div id="result"></div>
</body>
</html>
